<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ListItemRepository")
 */
class ListItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ToDoList", inversedBy="listItems")
     */
    private $ToDoList;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Unique
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getToDoList(): ?ToDoList
    {
        return $this->ToDoList;
    }

    public function setToDoList(?ToDoList $ToDoList): self
    {
        $this->ToDoList = $ToDoList;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


}
