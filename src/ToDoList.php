<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ToDoListRepository")
 */
class ToDoList
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="toDoLists")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ListItem", mappedBy="ToDoList")
     */
    private $listItems;

    public function __construct()
    {
        $this->listItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    /**
     * @return Collection|ListItem[]
     */
    public function getListItems(): Collection
    {
        return $this->listItems;
    }

    public function itemIsUnique(ListItem $listItemAdd): self
    {
        foreach ($this->listItems as $listItem) {
            if($listItem->name == $listItemAdd->name)
                $result = false


        return true;
    }

    public function addListItem(ListItem $listItem): self
    {
        if (!$this->listItems->contains($listItem)) {
            $this->listItems[] = $listItem;
            $listItem->setToDoList($this);
        }

        return $this;
    }

    public function removeListItem(ListItem $listItem): self
    {
        if ($this->listItems->contains($listItem)) {
            $this->listItems->removeElement($listItem);
            // set the owning side to null (unless already changed)
            if ($listItem->getToDoList() === $this) {
                $listItem->setToDoList(null);
            }
        }

        return $this;
    }

    public function canAddItem(Item $item)
    {
        if($this->canAddNewItem() && $this->isItemUnique($item) && $this->diffTimeItem())
            return $item;

        else
            return null ;
    }

    public function isItemUnique(Item $newItem)
    {
        $result = true;
        foreach ($this->items as $item) {
            if($item->name == $newItem->name)
                $result = false;
        }

        return $result;
    }

    public canAddItem(){
        if($this->getListItems()->count() > 10)
            return false;
        return true;
    }

    public function diffTimeItem()
    {
        $currentTime = new DateTime("now");
            if(count($this->items) == 0)
                return true;
            $lastItem = end($this->items);
            $lastItemTime = $lastItem->createdAt;
            $lastItemTime->modify("+30 minutes");
        return $currentTime >= $lastItemTime;

    }


}
