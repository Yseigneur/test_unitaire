<?php


use App\Entity\User;

class Usertest extends PHPUnit\Framework\TestCase
{


    public function testEmptyFirstName()
    {
        $user = new User("","Seigneur",21,"seigneur.yannick@gmail.com","azertyuiop");
        $this->assertEquals(false,$user->nameIsValid(),"Error firstname is empty");
    }

    public function testEmptyLastName()
    {
        $user = new User("yannick","",21,"seigneur.yannick@gmail.com","azertyuiop");
        $this->assertEquals(false,$user->nameIsValid(),"Error lastname is empty");
    }

    public function testEmptyFullName()
    {
        $user = new User("","",21,"seigneur.yannick@gmail.com","azertyuiop");
        $this->assertEquals(false,$user->nameIsValid(),"Error firstname  and lastname are empty");
    }





    public function testHigherValidAge()
    {
        $user = new User("yannick","Seigneur",21,"seigneur.yannick@gmail.com","azertyuiop");
        $this->assertEquals(true,$user->ageIsValid(),"Age is higher than 13");
    }

    public function testEqualsValidAge()
    {
        $user = new User("yannick","Seigneur",13,"seigneur.yannick@gmail.com","azertyuiop");
        $this->assertEquals(true,$user->ageIsValid(),"Age is 13");
    }

    public function testEmptyAge()
    {
        $user = new User("yannick","Seigneur",13,"seigneur.yannick@gmail.com","azertyuiop");
        $this->assertEquals(false,$user->ageIsValid(),"false age is lower than 13");
    }





    public function testValidEmail()
    {
        $user = new User("yannick","Seigneur",21,"seigneur.yannick@gmail.com","azertyuiop");
        $this->assertEquals(true,$user->emailIsValid(),"Return true if email is valid");
    }

    public function testEmptyEmail()
    {
        $user = new User("yannick","Seigneur",21,"","azertyuiop");
        $this->assertEquals(false,$user->emailIsValid(),"email is empty");
    }





    public function testValidPassword()
    {
        $user = new User("yannick","Seigneur",21,"seigneur.yannick@gmail.com","azertyuiop");
        $this->assertEquals(true,$user->passwordIsValid(),"Password is valid");
    }

       public function testEmptyPassword()
    {
        $user = new User("yannick","Seigneur",21,"seigneur.yannick@gmail.com","");
        $this->assertEquals(false,$user->passwordIsValid(),"Password is empty");
    }




    public function testValidUser()
    {
        $user = new User("yannick","Seigneur",21,"seigneur.yannick@gmail.com","azertyuiop");
        $this->assertEquals(true,$user->isValid(),"User is valid");
    }




    public function testCreateTodoList()
    {
        $user = new User("yannick","Seigneur",21,"seigneur.yannick@gmail.com","azertyuiop");
        $user->createTodoList();
        $this->assertEquals(true,$user->hasTodoList(),"Todolist is created");
    }


    public function testAddNewTodoList()
    {
        $user = new User("yannick","Seigneur",21,"seigneur.yannick@gmail.com","azertyuiop");
        $user->createTodoList();
        $this->assertEquals(false,$user->createTodoList(),"Error you already have a Todoliste");
    }





}
