<?php


use App\Entity\Item;
use App\Entity\Todolist;
use App\Entity\User;

class TodoListTest extends PHPUnit\Framework\TestCase
{

    //Item's Tests

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->user = new User("Yannick", "Seigneur", 21, "seigneur.yannick@gmail.com", "azertyuiop");
    }


    public function testCantAddItem()
    {
        $todoList = new Todolist($this->user);
        for ($i = 0; $i < 10; $i++) {
            $todoList->addItem(new Item("name" . $i, "content" . $i));
        }
        $this->assertEquals(false, $todoList->addListItem(), "Error you have 10 item or more");
    }

    public function testIsItemUnique()
    {
        $todoList = new Todolist($this->user);
        $todoList->addItem(new Item("name", "content"));
        $this->assertEquals(true, $todoList->itemIsUnique(New Item("name1", "content")), "Item is created");
    }

    public function testIsItemNotUnique()
    {
        $todoList = new Todolist($this->user);
        $todoList->addItem(new Item("name", "content"));
        $this->assertEquals(false, $todoList->itemIsUnique(New Item("name", "content")), "This item already exist");
    }

    public function testValidDiffTime()
    {
        $todoList = new Todolist($this->user);
        $item = new Item("name", "content");
        $item->createdAt->modify("-30 minutes");
        $todoList->addItem($item);
        $this->assertEquals(true, $todoList->diffTimeItem(), "Last item was created over 30 minutes ago");
    }


    public function testUnValidDiffTime()
    {
        $todoList = new Todolist($this->user);
        $item = new Item("name", "content");
        $todoList->addItem($item);
        $this->assertEquals(false, $todoList->diffTimeItem(), "last item was created less than 30 minutes ago");
    }


    public function testAddItem()
    {
        $todoList = new Todolist($this->user);
        $item = new Item("name", "content");
        $item->createdAt->modify("-30 minutes");
        $newitem = new Item("name1", "content");
        $todoList->addItem($item);
        $this->assertEquals($newitem, $todoList->canAddItem($newitem), "Item has been added");
    }


    public function testCannotAddItem()
    {
        $todoList = new Todolist($this->user);
        $item = new Item("name", "content");
        $newitem = new Item("name1", "content");
        $todoList->addItem($item);
        $this->assertEquals(null, $todoList->canAddItem($newitem), "This item cant be added");
    }

}
