<?php


use App\Entity\Email;
use App\Entity\User;

class EmailTest extends PHPUnit\Framework\TestCase
{

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->user = new User("Yannick", "Seigneur", 21, "seigneur.yannick@gmail.com", "azertyuiop");
    }

    public function SendMock()
    {
        $email = $this->createMock(Email::class);

        $email->expects($this->any())
            ->method('send')
            ->will($this->returnValue(false));

        $this->assertEquals(false,$email->send($this->user),"Email has been sent");
    }


    public function SendUser()
    {
        $email = new EmailService();
        $this->assertEquals(true,$email->send($this->user),"Email has been sent");
    }


    public function CantSendUser()
    {
        $email = new EmailService();
        $this->user->age = 17;
        $this->assertEquals(false,$email->send($this->user),"Email can't be send");
    }


}
